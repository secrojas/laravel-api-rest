<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\User\ListUserRequest;
use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\DeleteUserRequest;
use App\Http\Requests\User\ShowUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum');

        //To exclude some routes from the middleware
        // $this->middleware('auth:sanctum')->except(['index','show']);
    }

    public function index(ListUserRequest $request)
    {
        return UserResource::collection(User::all());
    }

    public function create(CreateUserRequest $request)
    {
        // $user = User::create($request->all());
        // return response()->json([
        //     'success' => true
        // ]);

        $user = new User;

        $user = $user->createModel($request);

        return new UserResource($user);
    }

    public function show(ShowUserRequest $request)
    {
        $user = User::findOrFail($request->user_id);

        return new UserResource($user);
    }

    public function update(UpdateUserRequest $request)
    {
        $user = User::findOrFail($request->user_id);

        $user = $user->updateModel($request);

        return new UserResource($user);
    }

    public function delete(DeleteUserRequest $request)
    {
        $user = User::findOrFail($request->user_id);

        $status = $user->deleteModel();

        return response()->json([
            'status' => $status,
            'user' => $user
        ]);
    }
}
